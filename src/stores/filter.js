import { readable } from "svelte/store";
import { getGazefilterContext } from "../ctx";

export default (function() {
  return readable(null, function start(set) {
    const { tracker } = getGazefilterContext();

    tracker.addListener("filter", set);

    return function stop() {
      tracker.removeListener("filter", set);
      set(null);
    };
  });
})();

