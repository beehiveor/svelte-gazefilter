import { readable } from "svelte/store";
import { getGazefilterContext } from "../ctx";

export default (function() {
  return readable({
    error: NaN,
    eye: 0,
    left: 0,
    right: 0,
  }, function start(set) {
    const { tracker } = getGazefilterContext();

    function update(response) {
      set({
        error: response.errorValue,
        eye: response.eye,
        left: response.leftCount,
        right: response.rightCount,
      });
    }

    tracker.addListener("calib", update);

    return function stop() {
      tracker.removeListener("calib", update);
    };
  });

})();
