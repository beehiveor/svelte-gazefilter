import { writable } from 'svelte/store';
import { getGazefilterContext } from '../ctx';

export default (function() {
  let promise,
      array,
      chunks = [],
      total = 0,
      loaded = 0;
  const { subscribe, set, update } = writable({ total, loaded, stage: "idle" });

  function setStage(stage) {
    set({ total, loaded, stage });
  };

  function updateLoaded() {
    update(state => ({ ...state, loaded, total }));
  }

  async function download(response) {
    total = +response.headers.get('Content-Length');
    setStage("get");

    if (total === null) {
      return response;
    };

    const reader = response.body.getReader();
    setStage("download");

    while (true) {
      const { done, value } = await reader.read();

      if (done) break;

      chunks.push(value);
      loaded += value.length;
      if (loaded > total) total = loaded;
      updateLoaded();
    }

    array = new Uint8Array(total);
    let position = 0;
    for (let chunk of chunks) {
      array.set(chunk, position);
      position += chunk.length;
    }
  }

  async function _init(url, options) {
    const { gazefilter: gf } = getGazefilterContext();

    const response = await fetch(url, options);
    await download(response);

    setStage("compile");
    await gf.init(array);
    array = null, chunks = [];

    setStage("ready");
  }

  return {
    subscribe,

    init(url, options = {}) {
      const { tracker } = getGazefilterContext();

      if (tracker.isReady()) return Promise.resolve();
      else return promise
        ? promise
        : (
          promise = _init(url, options)
            .finally(() => (promise = null))
        );
    },
  };
})();
