import { readable } from "svelte/store";
import { getGazefilterContext } from "../ctx";

export default (function() {
  return readable(false, function start(set) {
    const { tracker } = getGazefilterContext();

    function onchange(device) {
      if (device) set(false);
      else set(true);
    }

    tracker.addListener("pause", set);
    tracker.addListener("change", onchange);

    return async function stop() {
      tracker.removeListener("pause", set);
      tracker.removeListener("change", onchange);
    };
  });
})();

