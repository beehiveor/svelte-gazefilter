import { readable } from "svelte/store";
import { getGazefilterContext } from "../ctx";

export default (function() {
  return readable(undefined, function start(set) {
    const { tracker } = getGazefilterContext();

    set(tracker.deviceInfo());
    tracker.addListener("change", set);

    return function stop() {
      tracker.removeListener("change", set);
      set(undefined);
    };
  });
})();

