import { readable } from "svelte/store";
import { getGazefilterContext } from "../ctx";

export default (function() {
  return readable({ timestamp: NaN, x: NaN, y: NaN, duration: 0 }, function start(set) {
    let offsetY = window.screenY + 80, offsetX = window.screenX;

    function clear() {
      set({ timestamp: performance.now(), x: NaN, y: NaN, valid: false });
    }

    function onmouse(event) {
      offsetX = event.screenX - event.clientX;
      offsetY = event.screenY - event.clientY;
    }

    function onchange(device) {
      if (!device) clear();
    }

    function onpause(value) {
      if (value) clear();
    }

    function onfilter(event) {
      let [ gazeX, gazeY ] = event.fixationPoint();
      let shadowX = gazeX - offsetX;
      let shadowY = gazeY - offsetY;

      set({ timestamp: event.timestamp, x: shadowX, y: shadowY, duration: event.fixationDuration() });
    }

    const { tracker } = getGazefilterContext();

    window.addEventListener("mousemove", onmouse);
    tracker.addListener("filter", onfilter);
    tracker.addListener("change", onchange);
    tracker.addListener("pause", onpause);

    return function stop() {
      tracker.removeListener("filter", onfilter);
      tracker.removeListener("change", onchange);
      tracker.removeListener("pause", onpause);
      window.removeEventListener("mousemove", onmouse);
    };
  });
})();
