import { getContext, setContext } from "svelte";

const key = {};

export function getGazefilterContext() {
  return getContext(key);
}

export function setGazefilterContext(module) {
  setContext(key, {
    get gazefilter() { return module; },
    get version() { return module.version; },
    get tracker() { return module.tracker; },
    get visualizer() { return module.visualizer; },
    get recorder() { return module.recorder; },
    get findDevices() { return module.findDevices; },
  });
}
