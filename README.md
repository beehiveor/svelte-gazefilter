# svelte-gazefilter

[Cybernetically](https://svelte.dev/) enhanced eye-tracking with [gazefilter](https://beehiveor.gitlab.io/gazefilter/) 💪🔥

## Basics

- Initialization of gazefilter with download progress.
- Reactive tracking states: connect, pause, calibration, etc.
- Recorder component to wrap eye-tracking recording.

# Pseudo Example

```svelte
<!-- 1. Set gazefilter module -->
<Gazefilter {gazefilter}>

  <!-- 2. Connect to device -->
  <Tracker let:toggleConnect>

    <button on:click={toggleConnect}>toggle connect</button>

```

## Docs

### `<Gazefilter/>`

Sets *gazefilter* context.

Props:

- *gazefilter* library module. If empty it will attempt to use `window.gazefilter`.

Slots:

- *init* shown during initialization;
- *default* shown after gazefilter is initialized;
- *fallback* shown if any error occured.

Slot props:

- *init* store for initialization status:
  - *stage*: `idle` -> `download` -> `compile` -> `ready`;
  - *total* bytes to download (during `download` stage);
  - *loaded* bytes (during `download` stage).
- *error* in case initialization failed.

Events:

- *init* fired after init is done;
- *progress* fired during initialization with `{ stage, total, loaded }`.


### `<Tracker/>`

Listens to `gazefilter.tracker`.

Slots:

- *default* shown always;
- *connecting* shown during connecting process (only works using `toggleConnect`);
- *connected* shown if gazefilter is connected to any device;
- *disposed* shown if no device is connnected;

Slot props:

- *toggleConnect* function to connect or disconnect from device.
- *togglePause* function to pause or resume tracking.
- *device* current device `tracker.deviceInfo() | undefined`
- *pause* pause status `true | false`
- *calib* calibration status

Events:

- *pause* fired on tracker pause or resume;
- *change* fired on tracker connect or dispose;
- *calib* fired on tracker calibration event.

### `<Recorder/>`

> **NOTE:** In case `Recorder` component is used, do not call `gazefilter.recorder` methods. Any mutation done via direct library call will not be reflected by this component.

Slots:

- *default* shown always;
- *recording* shown during recording process;
- *loading* shown during loading process (start, stop async methods of recorder);
- *idle* shown if recorder ready to record.

Slot props:

- *start* async function to start recording. Signature same as for `gazefilter.recorder.start`;
- *stop* async function to stop recording. Signature same as for `gazefilter.recorder.stop`;
- *push* function to push custom event. Signature same as for `gazefilter.recorder.pushEvent`.

Events:

- *start* fired on start recording;
- *stop* fired on stop recordding;
- *batch* fired in case batched recording.
